'''
Columnas a seleccionar:
*Country -> index
*Language -> index
*Gross Earnings
'''
import pandas as pd
# ruta file csv
rutaFileCsv = 'https://github.com/luisguillermomolero/MisionTIC2022_2/blob/master/Modulo1_Python_MisionTIC2022_Main/Semana_5/Reto/movies.csv?raw=true'

def listaPeliculas(rutaFileCsv: str) -> str:
    try:
        #Cargar el archivo
        movies = pd.read_csv(rutaFileCsv)
        #Crear subCOnjunto
        subConjunto = movies.loc[:, ['Country','Language','Gross Earnings']]
        #Crear tabla dinámica
        tabla = pd.pivot_table(subConjunto, index=['Country','Language'])
        #Retornar los primeros 10 registros
        return tabla.head(10)
    except:
        return "Error al leer el archivo de datos."


print(listaPeliculas(rutaFileCsv))