'''
APUNTES:
*Desarrollar una función que reciba como parámetro un diccionario
    Estructura del diccionario a recibir: ✔️
    informacion = {
        'id_cliente': 0,
        'nombre': '',
        'edad': 0,
        'primer_ingreso': True
    }
*La función debe retornar un diccionario con la siguiente estructura: ✔️
    respuesta = {
        'nombre': '',
        'edad': 0,
        'atraccion': '',
        'primer_ingreso': True,
        'total_boleta': 0,
        'apto': True
    }

*apto será verdad SOLO si está dentro del rango de edad de la tabla ✔️
*atraccion y total_boleta -> 'N/A' si no está en rango de edad de la tabla ✔️
*primer_ingreso -> si es verdadero se aplica el descuento indicado en la tabla ✔️
'''

def cliente(informacion: dict)->dict:
    #Obtener la edad del cliente
    edad = informacion['edad']
    #Variables que darán valor al diccionario respuesta
    valor_boleta = 'N/A'
    atraccion = 'N/A'
    apto = False
    #Evaluar la edad del cliente
    if edad > 18:
        valor_boleta = 20000
        atraccion = 'X-Treme'
        apto = True
        if informacion['primer_ingreso'] == True:
            valor_boleta = valor_boleta - (valor_boleta * 0.05)
    elif edad >= 15:
            pass


    respuesta = {
        'nombre': informacion['nombre'],
        'edad': edad,
        'atraccion': atraccion,
        'primer_ingreso': informacion['primer_ingreso'],
        'apto': apto,
        'total_boleta': valor_boleta
    }
    return respuesta



otroDiccionario = {
        'id_cliente': 0,
        'nombre': '',
        'edad': 0,
        'primer_ingreso': True
    }

cliente(otroDiccionario)