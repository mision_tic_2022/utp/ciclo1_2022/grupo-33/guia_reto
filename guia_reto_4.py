from functools import reduce



def obtener_totales(rutina_contable):
    respuesta = '------------------------ Inicio Registro diario ---------------------------------\n'
    #Iterar rutina contable
    for lista in rutina_contable:
        #Sacar el elemento de la lista
        id = lista.pop(0)
        #
        obtener_total = lambda tupla: tupla[1] * tupla[2]
        #Mapear la lista de tuplas
        lista_total = list(map( obtener_total, lista ))
        #Obtener el total 
        total = reduce( lambda ac,e: ac+e, lista_total)
        #Concatenar al string
        respuesta += f'La factura {id} tiene un total en pesos de '
        if total < 600000:
            total += 25000
        respuesta += '{:,.2f}\n'.format(total)

    respuesta += '-------------------------- Fin Registro diario ---------------------------------'
    return respuesta

rutinaContable = [
    [1201, ("5464", 4, 25842.99), ("7854", 18, 23254.99), ("8521", 9, 48951.95)],
    [1202, ("8756", 3, 115362.58), ("1112", 18, 2354.99)],
    [1203, ("2547", 1, 125698.20), ("2635", 2, 135645.20),("1254", 1, 13645.20), ("9965", 5, 1645.20)],
    [1204, ("9635", 7, 11.99), ("7733", 11, 18.99), ("88112", 5, 390.95)]
]
print( obtener_totales(rutinaContable) )
'''
La factura 1201 tiene un total en pesos de 962,529.33
'''