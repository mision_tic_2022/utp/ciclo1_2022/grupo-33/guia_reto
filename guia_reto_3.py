'''
Desarrollar función que reciba como parámetro una lista de tuplas:
[
    (id_producto: int, desc_producto: str, num_producto: str, 
    cant_vendida: int, stock: int, nombre_comprador: str, doc_comprador: int,
    fecha_venta: str),()]

La función debe retornar un String con la siguiente estructura:
* Producto consultado : 2010 Descripción bujía #Parte MS9512 Cantidad vendida 4 Stock 15 Comprador Carlos Rondon Documento 1256 Fecha Venta 12/06/2020
* “No hay registro de venta de ese producto”

NOTA:
    *Permitir introducir ventas a la lista de tuplas que se recibe como parámetro,
    posteriormente convertirlo a un diccionario
'''