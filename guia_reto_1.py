'''
Genera intereses si: tiempo > 2
valor_interes = (cantidad * porcentaje_interes * tiempo) / 12
porcentaje_interes = 0.03
valor_total = valor_interes + cantidad
--------------
si tiempo <= 2 
porcentaje_perder = 0.02
valor_perder = cantidad * porcentaje_perder
valor_total = cantidad - valor_perder
---------
parámetros de la función:
    *usuario
    *capital
    *tiempo
    -Utilizar en RETURN para retornar una cadena
-----------
String para las ganancias:
Para el usuario {} La cantidad de dinero a recibir, según el monto inicial {} para un tiempo de {} meses es: {}

String para penalidad:
Para el usuario {} La cantidad de dinero a recibir, según el monto inicial {} para un tiempo de {} meses es: {}

'''

def CDT(usuario: str, capital: int, tiempo: int):
    mensaje = ""
    if tiempo > 2:
        porcentaje_interes = 0.03
        valor_interes = (capital * porcentaje_interes * tiempo) / 12
        #Aquí calcular valor total
        mensaje = f"Para el usuario {usuario} La cantidad de dinero a recibir, según el monto inicial {} para un tiempo de {tiempo} meses es: {}"
    else:
        porcentaje_perder = 0.02
        valor_perder = capital * porcentaje_perder
        #Aquí calcular valor total
        mensaje = f"Para el usuario {usuario} La cantidad de dinero a recibir, según el monto inicial {} para un tiempo de {tiempo} meses es: {}"
    return mensaje
